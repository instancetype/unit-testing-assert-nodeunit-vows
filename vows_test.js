/**
 * Created by instancetype on 6/19/14.
 */
var vows = require('vows')
  , assert = require('assert')
  , Todo = require('./todo')

vows.describe('Todo').addBatch(
  { 'when adding an item': { topic: function() {
                               var todo = new Todo()
                               todo.add('Cook Jitsu')

                               return todo
                           }
                           , 'it should exist in my todos': function(err, todo) {
                               assert.equal( todo.getCount(), 1)
                           }
  }
}).run() // standalone test script: $ node vows_test