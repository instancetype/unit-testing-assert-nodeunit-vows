/**
 * Created by instancetype on 6/19/14.
 */
var Todo = require('../todo')
  , todo = new Todo()

exports.testAdd = function(test) {
  test.expect(2)
  todo.add('item')
  test.equal(todo.getCount(), 1, 'Count should equal 1')
  test.equal(todo.todos[0], 'item', 'Item added should be \'item\'')
  test.done()
}

// To run all tests in nodeunit_test directory:
// $ nodeunit nodeunit_test